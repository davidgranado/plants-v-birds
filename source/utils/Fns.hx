package utils;

import components.render.SpriteComponent;

import edge.Entity;
import flixel.FlxSprite;

class Fns {
	static inline public function angleBetween(x1:Float, y1:Float, x2:Float, y2:Float) {
		return Math.atan2(
			y2 - y1,
			x2 - x1
		);
	}

	public inline static function degToRad(degrees:Float):Float {
		return Math.PI / 180 * degrees;
	}

	static inline public function radToDeg(radians:Float):Float {
		return 180 / Math.PI * radians;
	}

	static inline public function AABBCheck(rect1:FlxSprite, rect2:FlxSprite):Bool {
		return (
			rect1.x < rect2.x + rect2.width &&
			rect1.x + rect1.width > rect2.x &&
			rect1.y < rect2.y + rect2.height &&
			rect1.height + rect1.y > rect2.y
		);
	}

	@:access(edge.Entity.map)
	public static inline function getSprite(entity:Entity):FlxSprite {
		return (cast entity.map.get('components.render.SpriteComponent'):SpriteComponent).instance;
	}

	public static function matchSpriteToEntity(sprite:FlxSprite, entities:Array<Entity>):Entity {
		for(entity in entities) {
			if(sprite == Fns.getSprite(entity)) {
				return entity;
			}
		}

		return null;
	}
}
