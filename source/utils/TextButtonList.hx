package utils;

class TextButtonList {
	var buttons:Array<TextButton>;
	var activeButtonIndex = 0;

	public function new() {
		buttons = new Array<TextButton>();
	}

	public function get(index:Int) {
		return buttons[index];
	}

	public function set(index:Int, v:TextButton):TextButton {
		return buttons[index] = v;
	}


	public function highlightNext() {
		buttons[activeButtonIndex].isHighlighted = false;

		if(activeButtonIndex == buttons.length - 1) {
			activeButtonIndex = 0;
		} else {
			activeButtonIndex++;
		}

		buttons[activeButtonIndex].isHighlighted = true;
	}

	public function highlightPrev() {
		buttons[activeButtonIndex].isHighlighted = false;

		if(activeButtonIndex == buttons.length - 1) {
			activeButtonIndex--;
		} else {
			activeButtonIndex = 0;
		}

		buttons[activeButtonIndex].isHighlighted = true;
	}

	public function push(button:TextButton) {
		if(buttons.length == 0) {
			button.isHighlighted = true;
		}

		buttons.push(button);

		return buttons.length;
	}

	public function selectHighlighted():Void {
		buttons[activeButtonIndex].onSelect();
	}
}
