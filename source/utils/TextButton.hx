package utils;

import flixel.group.FlxGroup;
import flixel.text.FlxText;
import flixel.util.FlxColor;

class TextButton extends FlxTypedGroup<FlxText> {
	public var unhighlightedColor:FlxColor;
	public var fontSize:Int;
	public var highlightedColor:FlxColor;
	public var isHighlighted(get, set):Bool;
	public var label:FlxText;
	public var onSelect:Void -> Void;

	var highlighted = false;

	public function new(
		_x:Float,
		_y:Float,
		_label:String,
		_onSelect:Void -> Void,
		_unhighlightedColor = FlxColor.WHITE,
		_highlightedColor = FlxColor.RED,
		_fontSize = 16
	) {
		super(1);

		onSelect = _onSelect;
		label = new FlxText(_x, _y, _label);
		highlightedColor = _highlightedColor;
		unhighlightedColor = _unhighlightedColor;
		label.size = _fontSize;
		label.color = _unhighlightedColor;

		add(label);
	}

	public function get_isHighlighted():Bool {
		return highlighted;
	}

	public function set_isHighlighted(highlight:Bool):Bool {
		if(highlight == highlighted) {
			return highlighted;
		}

		highlighted = highlight;

		label.color = highlighted ? highlightedColor:unhighlightedColor;

		return highlighted;
	}
}
