package hud;

import entities.Player;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.group.FlxSpriteGroup;
import flixel.text.FlxText;
import flixel.util.FlxColor;

class Hud extends FlxSpriteGroup {
	public var levelText:FlxText;
	public var lifeBar:Bar;
	public var lifeLevelText:FlxText;
	public var lifeText:FlxText;
	public var powerBar:Bar;
	public var powerLevelText:FlxText;
	public var powerText:FlxText;
	public var time = 0.0;
	public var timeText:FlxText;

	var player:Player;

	public function new(_player:Player):Void {
		super();

		scrollFactor.set(0, 0);
		alpha = 0.7;

		player = _player;

		var fontSize = 18;
		var width = 95;

		timeText = new FlxText(FlxG.width - width, 8, width, 'Time:', fontSize);
		levelText = new FlxText(630, 20, width, 'Level: ', fontSize);
		lifeText = new FlxText(0, 32, width, 'Life:', fontSize);
		lifeLevelText = new FlxText(610, 32, width, '0', fontSize);
		powerText = new FlxText(0, 8, width, 'Power:', fontSize);
		powerLevelText = new FlxText(610, 8, width, '0', fontSize);

		lifeText.alignment = FlxTextAlign.RIGHT;
		powerText.alignment = FlxTextAlign.RIGHT;
		timeText.origin.set(FlxG.width);

		levelText.color =
		lifeText.color =
		lifeLevelText.color =
		powerText.color =
		powerLevelText.color =
		timeText.color =
			FlxColor.BLACK;

		add(timeText);
		add(levelText);
		add(lifeText);
		add(lifeLevelText);
		add(powerText);
		add(powerLevelText);

		powerBar = new Bar(100, 10, 500, FlxColor.RED);
		lifeBar = new Bar(100, 34, 500, FlxColor.GREEN);

		powerBar.setScale(0);

		add(powerBar);
		add(lifeBar);
	}

	override public function update(dt:Float):Void {
		time += dt;

		lifeBar.setScale(((player.life - 1) % 100 + 1) / 100);
		powerBar.setScale(((player.power - 1) % 100 + 1) / 100);
		lifeLevelText.text = '' + Std.int(player.life / 100);
		powerLevelText.text = '' + Std.int(player.power / 100);
		levelText.text = 'Level: ' + player.level;
		timeText.text = 'Time: ' + Std.int(time);

		super.update(dt);
	}
}
