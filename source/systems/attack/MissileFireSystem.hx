package systems.attack;

import components.render.SpriteComponent;
import components.attack.MissileLauncherComponent;
import entities.Missile;
import flixel.math.FlxAngle;
import utils.Fns;

import edge.ISystem;
import flixel.FlxG;

class MissileFireSystem implements ISystem {
	public function update(sprite:SpriteComponent, weapon:MissileLauncherComponent):Bool {
		if(!FlxG.mouse.justPressed) {
			return true;
		}

		var mousePosition = FlxG.mouse.getPosition();

		Missile.create(sprite.instance.x, sprite.instance.y, mousePosition.x, mousePosition.y);

		return true;
	}
}
