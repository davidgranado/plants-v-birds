package systems.attack;

import components.render.PositionComponent;
import components.attack.GunComponent;
import entities.Bullet;
import utils.Fns;

import edge.ISystem;
import flixel.FlxG;

class GunFireSystem implements ISystem {
	var timeDelta:Float;

	public function update(position:PositionComponent, gun:GunComponent):Bool {
		gun.remainingCooldownTime = Math.max(
			0,
			gun.remainingCooldownTime - timeDelta
		);

		if(!FlxG.mouse.pressed) {
			gun.recoil = 0;
			return true;
		}

		if(
			gun.remainingCooldownTime > 0 ||
			!gun.automatic && !FlxG.mouse.justPressed
		) {
			return true;
		}

		var mousePosition = FlxG.mouse.getPosition();
		var recoilMod = gun.recoil * Math.random() - (gun.recoil / 2);
		var angle = Fns.angleBetween(position.x, position.y, mousePosition.x, mousePosition.y) + Fns.degToRad(recoilMod);
		Bullet.create(position.x, position.y, angle);
		gun.remainingCooldownTime = gun.cooldownTime;

		if (gun.recoil == 0) {
			gun.recoil = gun.recoilPower;
		} else {
			gun.recoil = Math.min(gun.maxRecoil, gun.recoil + (gun.recoilPower * gun.additionalRecoilCoefficient));
		}

		return true;
	}
}
