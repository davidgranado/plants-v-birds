package systems.attack;

import components.render.SpriteComponent;
import components.movement.PhysicsBodyComponent;
import components.attack.TargetComponent;
import entities.Explosion;

import edge.Entity;
import edge.ISystem;
import flixel.FlxG;
import flixel.math.FlxPoint;

class TargetDetonationSystem implements ISystem {
	var entity:Entity;

	public function update(target:TargetComponent, sprite:SpriteComponent):Bool {
		if(sprite.instance.getHitbox().containsPoint(new FlxPoint(target.x, target.y))) {
			Explosion.create(target.x, target.y);
			entity.destroy();
		}

		return true;
	}
}
