package systems.attack;

import utils.Fns;
import components.attack.PlayerAttackComponent;
import components.attack.PlayerDamagableComponent;
import components.attack.KillableComponent;
import components.render.SpriteComponent;

import edge.Entity;
import edge.ISystem;
import edge.View;
import edge.ViewData;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.group.FlxGroup;

class PlayerAttackSystem implements ISystem {
	var playerAttacksGroup:FlxGroup;
	var playerDamagablesGroup:FlxGroup;
	var playerAttacks:View<{
		sprite:SpriteComponent,
		playerAttack:PlayerAttackComponent
	}>;
	var playerDamagables:View<{
		playerDamagable:PlayerDamagableComponent,
		killable:KillableComponent,
		sprite:SpriteComponent
	}>;

	public function new(playerAttacksGroup, playerDamagablesGroup) {
		this.playerAttacksGroup = playerAttacksGroup;
		this.playerDamagablesGroup = playerDamagablesGroup;
	}

	public function update():Bool {
		FlxG.overlap(playerAttacksGroup, playerDamagablesGroup, overlapPlayerAttackVsEnemy, Fns.AABBCheck);
		return true;
	}

	function overlapPlayerAttackVsEnemy(playerAttackSprite:FlxSprite, playerDamagableSprite:FlxSprite):Bool {
		var collidedPlayerAttack = null;

		for(playerAttack in playerAttacks) {
			if(playerAttack.data.sprite.instance == playerAttackSprite) {
				collidedPlayerAttack = playerAttack;
				continue;
			}
		}

		if(collidedPlayerAttack == null) {
			return true;
		}

		var collidedPlayerDamagable = null;

		for(playerDamagable in playerDamagables) {
			if(playerDamagable.data.sprite.instance == playerDamagableSprite) {
				collidedPlayerDamagable = playerDamagable;
				continue;
			}
		}

		if(collidedPlayerDamagable == null) {
			return true;
		}

		collidedPlayerDamagable.data.killable.life -= collidedPlayerAttack.data.playerAttack.power;
		collidedPlayerAttack.entity.destroy();

		return true;
	}

	public function playerAttacksAdded(entity:Entity, data:{
		var sprite:SpriteComponent;
		var playerAttack:PlayerAttackComponent;
	}) {
		trace('adding attack');
		playerAttacksGroup.add(data.sprite.instance);
	}

	public function playerAttacksRemoved(entity:Entity, data:{
		var sprite:SpriteComponent;
		var playerAttack:PlayerAttackComponent;
	}) {
		trace('removing attack');
		playerAttacksGroup.remove(data.sprite.instance);
	}

	public function playerDamagablesAdded(entity:Entity, data:{
		var playerDamagable:PlayerDamagableComponent;
		var killable:KillableComponent;
		var sprite:SpriteComponent;
	}) {
		trace('adding damagable');
		// playerDamagablesGroup.add(data.sprite.instance);
	}

	public function playerDamagablesRemoved(entity:Entity, data:{
		var playerDamagable:PlayerDamagableComponent;
		var killable:KillableComponent;
		var sprite:SpriteComponent;
	}) {
		trace('remove damagable');
		// playerDamagablesGroup.remove(data.sprite.instance);
	}
}
