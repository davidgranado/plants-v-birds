package systems.attack;

import components.attack.ExplosionComponent;
import components.render.PositionComponent;
import components.render.SpriteComponent;

import edge.Entity;
import edge.ISystem;

class ExplosionSystem implements ISystem {
	var rate = 1.0;
	var entity:Entity;
	var timeDelta:Float;

	public function update(explosion:ExplosionComponent, sprite:SpriteComponent, position:PositionComponent):Bool {
		var direction = explosion.expanding ? 1 : -1;

		explosion.currentRadius += explosion.power * direction * (timeDelta / 1000);

		if(explosion.expanding) {
			if(explosion.currentRadius >= 10) {
				explosion.expanding = false;
				return true;
			}
		} else if(explosion.currentRadius <= 0) {
			entity.destroy();
			return true;
		}

		var originalWidth = sprite.instance.width;
		var originalHeight = sprite.instance.height;
		sprite.instance.scale.set(explosion.currentRadius, explosion.currentRadius);
		sprite.instance.updateHitbox();
		position.x -= (sprite.instance.width - originalWidth)/2;
		position.y -= (sprite.instance.height - originalHeight)/2;
		return true;
	}
}
