package systems;

import components.render.PositionComponent;
import components.attack.TurretSpawnerComponent;
import entities.Turret;

import edge.Entity;
import edge.ISystem;
import flixel.FlxG;

class TurretSpawnSystem implements ISystem {
	public function update(position:PositionComponent, turretSpawnerw:TurretSpawnerComponent):Bool {
		if(FlxG.keys.justPressed.SPACE) {
			Turret.create(position.x);
		}

		return true;
	}
}
