package systems;

import components.attack.EnemySpawnerComponent;
import entities.enemies.Carrier;

import edge.ISystem;
import flixel.FlxG;

class EnemySpawnControlSystem implements ISystem {
	static var SPAWN_BAND_HEIGHT = 200.0;
	static var SPAWN_BAND_OFFSET = 50.0;
	static var SPAWN_ORIGIN_LEFT:Float;
	static var SPAWN_ORIGIN_RIGHT:Float;

	var timeDelta:Float;

	public function new() {
		var padding = 200;

		SPAWN_ORIGIN_LEFT = 0 - padding;
		SPAWN_ORIGIN_RIGHT = FlxG.width + padding;
	}

	public function update(enemySpawner:EnemySpawnerComponent):Bool {
		enemySpawner.remainingTimeToSpawn -= timeDelta;

		if(enemySpawner.remainingTimeToSpawn <= 0) {
			var x = (Math.random() < 0.5) ? SPAWN_ORIGIN_LEFT : SPAWN_ORIGIN_RIGHT;
			var y = SPAWN_BAND_OFFSET + (Math.random() * SPAWN_BAND_HEIGHT);

			Carrier.create(x, y);

			enemySpawner.remainingTimeToSpawn = enemySpawner.spawnRate;
		}

		return true;
	}
}
