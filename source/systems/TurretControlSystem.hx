package systems;

import components.EquipmentSlotsComponent;
import components.GrabableComponent;
import components.attack.GunComponent;
import components.PlayerComponent;
import components.movement.PhysicsBodyComponent;
import components.render.SpriteComponent;
import entities.Player;
import states.PlayState;

import edge.Entity;
import edge.ISystem;
import edge.View;
import flixel.FlxG;
import flixel.FlxSprite;

class TurretControlSystem implements ISystem {
	var grabableItems:View<{
		sprite:SpriteComponent,
		grabable:GrabableComponent
	}>;

	public function update(
		player:PlayerComponent,
		sprite:SpriteComponent,
		physicsBody:PhysicsBodyComponent,
		equipmentSlots:EquipmentSlotsComponent
	):Bool {
		if(!FlxG.keys.justPressed.E) {
			return true;
		}

		if(equipmentSlots.turret == null) {
			for(item in grabableItems) {
				if(touching(item.data.sprite.instance, sprite.instance)) {
					physicsBody.immovable = true;
					item.entity.add(new GunComponent());
					equipmentSlots.turret = item.entity;

					return true;
				}
			}
		} else {
			physicsBody.immovable = false;
			physicsBody.acceleration.x = 0;
			physicsBody.velocity.x = 0;
			equipmentSlots.turret.removeType(GunComponent);
			equipmentSlots.turret = null;
		}

		return true;
	}

	function touching(item:FlxSprite, player:FlxSprite):Bool {
		return
			item.x + item.width > player.x &&
			item.x < player.x + player.width;
	}
}
