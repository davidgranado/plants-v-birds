package systems;

import components.DiesOffscreenComponent;
import components.movement.PhysicsBodyComponent;
import components.render.SpriteComponent;
import states.PlayState;

import edge.Entity;
import edge.ISystem;
import flixel.FlxSprite;

class HeadingOffscreenKillSystem implements ISystem {
	static var PADDING = 100;

	var entity:Entity;

	public function update(sprite:SpriteComponent, physicsBody:PhysicsBodyComponent, diesOffscreen:DiesOffscreenComponent):Bool {
		if(
			isLeavingLeft(sprite.instance, physicsBody) ||
			isLeavingRight(sprite.instance, physicsBody) ||
			isLeavingTop(sprite.instance, physicsBody)
		) {
			entity.destroy();
		}

		return true;
	}

	inline function isLeavingLeft(sprite:FlxSprite, physicsBody:PhysicsBodyComponent):Bool {
		return  physicsBody.velocity.x < 0 && (sprite.x + sprite.width < -PADDING);
	}

	inline function isLeavingRight(sprite:FlxSprite, physicsBody:PhysicsBodyComponent):Bool {
		return  physicsBody.velocity.x > 0 && (sprite.x > PlayState.LEVEL_WIDTH + PADDING);
	}

	inline function isLeavingTop(sprite:FlxSprite, physicsBody:PhysicsBodyComponent):Bool {
		return  physicsBody.velocity.y < 0 && (sprite.y + sprite.height < -PADDING);
	}
}
