package systems;

import components.render.PositionComponent;
import components.render.SpriteComponent;
import states.PlayState;

import edge.Entity;
import edge.ISystem;
import flixel.FlxG;

class RenderingSystem implements ISystem {
	public function update(position:PositionComponent, sprite:SpriteComponent):Bool {
		if(sprite.instance.x != position.x) {
			sprite.instance.x = position.x;
		}

		if(sprite.instance.y != position.y) {
			sprite.instance.y = position.y;
		}

		return true;
	}

	public function updateAdded(entity:Entity, data: { position:PositionComponent, sprite:SpriteComponent }) {
		update(data.position, data.sprite);
	}

	public function updateRemoved(entity:Entity, data: { position:PositionComponent, sprite:SpriteComponent }) {
		update(data.position, data.sprite);
		// TODO Figure out a better way to do this cleanup
		(cast FlxG.state:PlayState).entitiesPlayerAttacks.remove(entity);
		(cast FlxG.state:PlayState).entitiesEnemies.remove(entity);
		// END TODO
		data.sprite.instance.destroy();	// TODO: Implement pooling
	}
}
