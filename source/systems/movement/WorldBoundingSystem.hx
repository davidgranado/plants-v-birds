package systems.movement;

import components.render.PositionComponent;
import components.movement.PhysicsBodyComponent;
import components.render.SpriteComponent;
import components.movement.WorldBoundComponent;

import edge.Entity;
import edge.ISystem;
import flixel.FlxG;

class WorldBoundingSystem implements ISystem {
	public function update(position:PositionComponent, sprite:SpriteComponent, physicsBody:PhysicsBodyComponent, worldBounded:WorldBoundComponent):Bool {
		if (position.x < 0) {
			physicsBody.velocity.x = 0;
			position.x = 0;
		} else if (position.x > FlxG.worldBounds.width - sprite.instance.width) {
			physicsBody.velocity.x = 0;
			position.x = FlxG.worldBounds.width - sprite.instance.width;
		}

		if (position.y < 0) {
			physicsBody.velocity.y = 0;
			position.y = 0;
		} else if (position.y > FlxG.worldBounds.height - sprite.instance.height) {
			physicsBody.velocity.y = 0;
			position.y = FlxG.worldBounds.height - sprite.instance.height;
		}

		return true;
	}
}
