package systems.movement;

import components.movement.GravityComponent;
import components.movement.PhysicsBodyComponent;
import edge.ISystem;
import flixel.math.FlxVelocity;

class GravitySystem implements ISystem {
	static public var GRAVITY = 500.0;

	public function update(position:GravityComponent, physicsBody:PhysicsBodyComponent):Bool {
		physicsBody.acceleration.y = GRAVITY;

		return true;
	}
}
