package systems.movement;

import components.render.PositionComponent;
import components.render.SpriteComponent;
import components.movement.WorldWrapComponent;

import edge.ISystem;
import flixel.FlxG;

class WorldWrapSystem implements ISystem {
	public function update(position:PositionComponent, sprite:SpriteComponent, worldWrap:WorldWrapComponent):Bool {
		if(position.x >= FlxG.worldBounds.width) {
			position.x = -sprite.instance.width;
		}

		return true;
	}
}
