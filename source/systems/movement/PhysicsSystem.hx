package systems.movement;

import components.render.PositionComponent;
import components.movement.PhysicsBodyComponent;

import edge.Entity;
import edge.ISystem;
import flixel.math.FlxVelocity;

class PhysicsSystem implements ISystem {
	var entity:edge.Entity;
	var timeDelta:Float; // ms

	public function update(position:PositionComponent, physicsBody:PhysicsBodyComponent):Bool {
		if(physicsBody.immovable) {
			return true;
		}
		var timeDeltaSeconds = timeDelta/1000;
		var acceleration = physicsBody.acceleration;
		var drag = physicsBody.drag;
		var maxVelocity = physicsBody.maxVelocity;
		var velocity = physicsBody.velocity;

		if(acceleration.x != 0 || drag.x != 0) {
			velocity.x = FlxVelocity.computeVelocity(velocity.x, acceleration.x, drag.x, maxVelocity.x, timeDeltaSeconds);
		}

		if(velocity.x != 0) {
			position.x += velocity.x * timeDeltaSeconds;
		}

		if(acceleration.y != 0 || drag.y != 0) {
			velocity.y = FlxVelocity.computeVelocity(velocity.y, acceleration.y, drag.y, maxVelocity.y, timeDeltaSeconds);
		}

		if(velocity.y != 0) {
			position.y += velocity.y * timeDeltaSeconds;
		}

		return true;
	}
}
