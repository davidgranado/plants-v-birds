package systems;

import components.movement.PhysicsBodyComponent;
import components.PlayerComponent;
import components.render.PositionComponent;

import edge.Entity;
import edge.ISystem;
import flixel.FlxG;
import flixel.math.FlxPoint;


class PlayerControlSystem implements ISystem {
	var entity:Entity;

	public function update(
		position:PositionComponent,
		physicsBody:PhysicsBodyComponent,
		player:PlayerComponent
	):Bool {
		var isPowered = true; // player.power > 0;
		var acceleration = 0.0;

		if(!FlxG.mouse.pressed) {
			if(FlxG.gamepads.lastActive == null) {
				if(FlxG.keys.pressed.D) {
					acceleration = player.speed;
				} else if(FlxG.keys.pressed.A) {
					acceleration = -player.speed;
				}
			} else {
				acceleration = player.speed * FlxG.gamepads.lastActive.analog.value.LEFT_STICK_X;
			}
		}

		if(physicsBody.acceleration.x != acceleration) {
			physicsBody.acceleration.x = acceleration;
		}

		return true;
	}
}
