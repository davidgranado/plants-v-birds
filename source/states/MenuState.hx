package states;

import states.PlayState;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.util.FlxColor;
import flixel.math.FlxMath;
import utils.TextButton;
import utils.TextButtonList;

enum Buttons {
	Play;

	#if !(js || flash)
		Quit;
	#end
}

class MenuState extends FlxState {
	var fontWidth = 400.0;
	var playButton:TextButton;
	var quitButton:TextButton;
	var verticalOffset = 120.0;
	var buttons:TextButtonList;
	var activeButtonIndex = 0;

	override public function create():Void {
		super.create();

		buttons = new TextButtonList();

		var centerX = (FlxG.width - fontWidth) / 2;
		var title = new FlxText(centerX, verticalOffset, fontWidth, "Power Prototype");

		title.alignment = CENTER;
		title.size = 35;

		add(title);

		FlxG.camera.bgColor = FlxColor.BLUE;

		playButton = new TextButton(
			centerX,
			verticalOffset + 100,
			"Play",
			startPlay
		);

		playButton.label.alignment = CENTER;
		buttons.push(playButton);

		add(playButton);

		#if !(js || flash)
			quitButton = new TextButton(
				centerX,
				verticalOffset + 150,
				"Quit",
				quitGame
			);
			quitButton.label.alignment = CENTER;
			buttons.push(quitButton);

			add(quitButton);
		#end

		buttons.get(activeButtonIndex).isHighlighted = true;
	}

	override public function update(elapsed:Float):Void
	{
		updateInput();

		super.update(elapsed);
	}

	public function startPlay() {
		FlxG.switchState(new PlayState());
	}

	#if !(js || flash)
		public function quitGame() {
			Sys.exit(0);
		}
	#end

	public function updateInput() {
		if(FlxG.gamepads.lastActive == null) {
			if(FlxG.keys.justPressed.UP) {
				buttons.highlightPrev();
			} else if(FlxG.keys.justPressed.DOWN) {
				buttons.highlightNext();
			}

			if(FlxG.keys.justPressed.ENTER) {
				buttons.selectHighlighted();
			}
		} else {
			if(FlxG.gamepads.lastActive.analog.justMoved.LEFT_STICK_Y) {
				FlxG.gamepads.lastActive.analog.value.LEFT_STICK_Y > 0 ?
					buttons.highlightPrev() :
					buttons.highlightNext();
			}

			if(FlxG.gamepads.lastActive.justPressed.A) {
				buttons.selectHighlighted();
			}
		}
	}
}
