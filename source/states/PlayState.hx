package states;

import components.render.SpriteComponent;
import entities.*;
import entities.enemies.*;
import systems.*;
import systems.attack.*;
import systems.movement.*;
import utils.Fns;

import edge.Entity;
import edge.Engine;
import edge.Phase;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.group.FlxGroup;
import flixel.group.FlxGroup;
import flixel.math.FlxRect;
import flixel.util.FlxColor;

class PlayState extends FlxState {
	static var DEBUG = false;

	public static var LEVEL_WIDTH = 1280;
	public static var LEVEL_HEIGHT = 720;

	public var background:FlxGroup;
	public var playerAttacks:FlxGroup;
	public var enemies:FlxGroup;
	public var engine:Engine;
	public var entitiesEnemies:Array<Entity>;
	public var entitiesPlayerAttacks:Array<Entity>;
	public var turrets:FlxGroup;
	public var playerSprite:FlxSprite;
	public var powerups:FlxGroup;
	public var portals:FlxGroup;

	var cloudCount = 16;
	var deadzone:Float = 100.0;
	var foreground:FlxGroup;
	var groundSprite:FlxSprite;
	var paused = false;
	var systems:Phase;

	@:access(edge.Entity.map)
	override public function create():Void {
		super.create();

		FlxG.worldBounds.setSize(PlayState.LEVEL_WIDTH, PlayState.LEVEL_HEIGHT);
		background = new FlxGroup();
		enemies = new FlxGroup();
		engine = new Engine();
		entitiesEnemies = new Array<Entity>();
		entitiesPlayerAttacks = new Array<Entity>();
		groundSprite = Ground.create();
		playerAttacks = new FlxGroup();
		powerups = new FlxGroup();
		portals = new FlxGroup();
		turrets = new FlxGroup();

		var player = Player.create();
		playerSprite = Fns.getSprite(player);

		EnemyController.create();

		add(background);
		add(groundSprite);
		add(playerSprite);
		add(enemies);
		add(turrets);
		add(portals);
		add(playerAttacks);

		systems = engine.createPhase();
		systems.add(new WorldWrapSystem());
		systems.add(new GravitySystem());
		systems.add(new PlayerControlSystem());
		systems.add(new GunFireSystem());
		systems.add(new MissileFireSystem());
		systems.add(new TurretSpawnSystem());
		systems.add(new TurretControlSystem());
		systems.add(new EnemySpawnControlSystem());
		systems.add(new TargetDetonationSystem());
		systems.add(new ExplosionSystem());
		systems.add(new PhysicsSystem());
		systems.add(new PlayerAttackSystem(playerAttacks, enemies));
		systems.add(new WorldBoundingSystem());
		systems.add(new HeadingOffscreenKillSystem());
		systems.add(new RenderingSystem());

		camera.bgColor =  FlxColor.fromRGB(108, 140, 185);
		camera.target = playerSprite;
		camera.followLead.set(10, 10);
		camera.followLerp = 0.1;
		camera.deadzone = new FlxRect((camera.width - deadzone) / 2, (camera.height - deadzone) / 2, deadzone, deadzone);
		camera.setScrollBoundsRect(0, 0, PlayState.LEVEL_WIDTH, PlayState.LEVEL_HEIGHT);
	}

	override public function update(dt:Float):Void {
		if(FlxG.keys.justPressed.P) {
			if(paused) {
				paused = false;
			} else {
				paused = true;
			}
		}

		if(FlxG.keys.justPressed.F) {
			FlxG.fullscreen = !FlxG.fullscreen;
		}

		if(FlxG.keys.justPressed.O) {
			DEBUG = !DEBUG;
		}

		if(!paused) {
			if(DEBUG) {
				if(FlxG.keys.justPressed.N) {
					systems.update(16);
				}
			} else {
				systems.update(dt*1000); // TODO Can this constant be removed?
			}
		}
	}
}
