package entities;

import components.BeamFireComponent;
import components.EquipmentSlotsComponent;
import components.movement.PhysicsBodyComponent;
import components.PlayerComponent;
import components.render.PositionComponent;
import components.render.SpriteComponent;
import components.attack.TurretSpawnerComponent;
import components.movement.WorldBoundComponent;
import entities.Ground;
import states.PlayState;

import edge.IComponent;
import edge.Engine;
import edge.Entity;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.math.FlxPoint;
import flixel.util.FlxColor;

class Player {
	public static function create():Entity {
		var physicsBody = new PhysicsBodyComponent();
		var spriteWidth = 48;
		var spriteHeight = 64;
		var sprite = new FlxSprite();

		physicsBody.maxVelocity.set(750, 0);
		physicsBody.drag.set(700, 0);
		sprite.makeGraphic(spriteWidth, spriteHeight, FlxColor.YELLOW);
		sprite.origin.set(spriteWidth / 2, 0);

		return (cast FlxG.state:PlayState).engine.create([
				new EquipmentSlotsComponent(),
				new PlayerComponent(1200),
				new PositionComponent(500, PlayState.LEVEL_HEIGHT - Ground.THICKNESS - spriteHeight),
				new SpriteComponent(sprite),
				physicsBody,
				new TurretSpawnerComponent(),
				new WorldBoundComponent()
			]);
	}
}
