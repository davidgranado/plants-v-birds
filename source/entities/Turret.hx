package entities;

import components.GrabableComponent;
import components.render.PositionComponent;
import components.render.SpriteComponent;
import entities.Ground;
import states.PlayState;

import edge.Engine;
import edge.Entity;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.util.FlxColor;

class Turret {
	public static function create(x:Float):Entity {
		var spriteWidth = 12;
		var spriteHeight = 48;
		var sprite = new FlxSprite();
		var y = PlayState.LEVEL_HEIGHT - Ground.THICKNESS - spriteHeight;

		sprite.makeGraphic(spriteWidth, spriteHeight, FlxColor.GREEN);
		sprite.origin.set(spriteWidth / 2, 0);
		(cast FlxG.state:PlayState).turrets.add(sprite);

		return (cast FlxG.state:PlayState).engine.create([
				new GrabableComponent(),
				new PositionComponent(x, y),
				new SpriteComponent(sprite)
			]);
	}
}
