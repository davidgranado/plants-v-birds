package entities;

import components.DiesOffscreenComponent;
import components.movement.PhysicsBodyComponent;
import components.render.PositionComponent;
import components.render.SpriteComponent;
import components.attack.TargetComponent;
import states.PlayState;
import utils.Fns;

import edge.IComponent;
import edge.Engine;
import edge.Entity;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.util.FlxColor;

class Missile {
	public static function create(x:Float, y:Float, targetX:Float, targetY:Float):Entity {
		var physicsBody = new PhysicsBodyComponent();
		var sprite = new FlxSprite();
		var spriteSize = 48;
		var angle = Fns.angleBetween(x, y, targetX, targetY);

		physicsBody.velocity.x = 200 * Math.cos(angle);
		physicsBody.velocity.y = 200 * Math.sin(angle);
		sprite.makeGraphic(
			spriteSize,
			spriteSize,
			FlxColor.YELLOW,
			true
		);

		(cast FlxG.state:PlayState).playerAttacks.add(sprite);

		return (cast FlxG.state:PlayState).engine.create([
			physicsBody,
			new DiesOffscreenComponent(),
			new PositionComponent(x - sprite.width/2, y - sprite.height/2),
			new SpriteComponent(sprite),
			new TargetComponent(targetX, targetY)
		]);
	}
}