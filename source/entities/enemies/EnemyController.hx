package entities.enemies;

import components.attack.EnemySpawnerComponent;
import entities.Ground;
import states.PlayState;

import edge.IComponent;
import edge.Engine;
import edge.Entity;
import flixel.FlxG;

class EnemyController {
	public static function create():Entity {
		return (cast FlxG.state:PlayState).engine.create([
				new EnemySpawnerComponent()
			]);
	}
}
