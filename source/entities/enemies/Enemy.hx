package entities.enemies;

import components.DiesOffscreenComponent;
import components.movement.PhysicsBodyComponent;
import components.render.PositionComponent;
import components.render.SpriteComponent;
import states.PlayState;

import edge.IComponent;
import edge.Engine;
import edge.Entity;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.math.FlxAngle;
import flixel.math.FlxPoint;
import flixel.util.FlxColor;

class Enemy {
	static public var SPEED_VARIABILITY = 50.0;
	static public var BASE_SPEED = 100.0;

	public static function create(x:Float, y:Float):Entity {
		var fromLeft = (x < 0);
		var physics:PhysicsBodyComponent;
		var speed =	(Math.random() - 1) * SPEED_VARIABILITY + BASE_SPEED;
		var spriteWidth = 48;
		var spriteHeight = 48;
		var sprite = new FlxSprite();

		physics = new PhysicsBodyComponent();
		physics.velocity.x = fromLeft ? speed : -speed;

		sprite.makeGraphic(spriteWidth, spriteHeight, FlxColor.GREEN);
		(cast FlxG.state:PlayState).enemies.add(sprite);

		var enemy = (cast FlxG.state:PlayState).engine.create([
				new PositionComponent(x, y),
				new SpriteComponent(sprite),
				new DiesOffscreenComponent(),
				physics
			]);

		(cast FlxG.state:PlayState).entitiesEnemies.push(enemy);

		return enemy;
	}
}
