package entities;

import components.attack.ExplosionComponent;
import components.render.PositionComponent;
import components.render.SpriteComponent;
import states.PlayState;
import utils.Fns;

import edge.IComponent;
import edge.Engine;
import edge.Entity;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.util.FlxColor;

class Explosion {
	public static function create(x:Float, y:Float):Entity {
		var sprite = new FlxSprite();
		var spriteSize = 48;

		sprite.makeGraphic(
			spriteSize,
			spriteSize,
			FlxColor.YELLOW,
			true
		);
		sprite.alpha = 0.5;
		(cast FlxG.state:PlayState).playerAttacks.add(sprite);

		return (cast FlxG.state:PlayState).engine.create([
			new ExplosionComponent(10),
			new PositionComponent(x - sprite.width/2, y - sprite.height/2),
			new SpriteComponent(sprite)
		]);
	}
}
