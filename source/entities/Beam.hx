package entities;

import components.movement.PhysicsBodyComponent;
import components.PlayerComponent;
import components.render.PositionComponent;
import components.render.SpriteComponent;
import entities.Ground;
import states.PlayState;

import edge.IComponent;
import edge.Engine;
import edge.Entity;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.math.FlxPoint;
import flixel.util.FlxColor;

class Beam {
	public static function create(x:Float, y:Float, angle:Float):Entity {
		var beamSprite = new FlxSprite();
		var spriteWidth = 2000;
		var spriteHeight = 50;

		beamSprite.makeGraphic(
			spriteWidth,
			spriteHeight,
			FlxColor.BLUE,
			true
		);
		beamSprite.origin.set(0, spriteHeight/2);
		beamSprite.angle = angle - 90;
		(cast FlxG.state:PlayState).playerAttacks.add(beamSprite);

		return (cast FlxG.state:PlayState).engine.create([
				new PositionComponent(x + 24, y),
				new SpriteComponent(beamSprite)
			]);
	}
}
