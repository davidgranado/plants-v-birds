package entities;

import flixel.FlxG;
import flixel.util.FlxColor;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.math.FlxAngle;

class Powerup extends FlxSprite {
	public var power = 1.0;

	var aggressiveness:Float;
	var size = 10;
	var speed = 500;
	var warningExpirationTime = 3.0;
	var warningStartTime = 2.0;

	public function new(_x, _y) {
		super(_x, _y);

		makeGraphic( size, size, FlxColor.PURPLE);

		cast(FlxG.state, PlayState).powerups.add(this);
	}

	override public function update(dt:Float):Void {
		var angle:Float;

		if(warningStartTime > 0) {
			warningStartTime -= dt;
		} else if(warningExpirationTime > 0) {
			warningExpirationTime -= dt;

			visible = !visible;

			if(warningExpirationTime <= 0) {
				kill();
			}
		}

		angle = FlxAngle.angleBetween(this, cast (FlxG.state, PlayState).player);

		velocity.x = speed * Math.cos(angle);
		velocity.y = speed * Math.sin(angle);

		super.update(dt);
	}
}
