package entities;

import components.DiesOffscreenComponent;
import components.attack.PlayerAttackComponent;
import components.movement.PhysicsBodyComponent;
import components.render.PositionComponent;
import components.render.SpriteComponent;
import states.PlayState;
import utils.Fns;

import edge.IComponent;
import edge.Engine;
import edge.Entity;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.util.FlxColor;

class Bullet {
	public static function create(x:Float, y:Float, angle:Float):Entity {
		var physicsBody = new PhysicsBodyComponent();
		var sprite = new FlxSprite();
		var spriteSize = 24;

		physicsBody.velocity.x = 2000 * Math.cos(angle);
		physicsBody.velocity.y = 2000 * Math.sin(angle);
		sprite.makeGraphic(
			spriteSize,
			spriteSize,
			FlxColor.YELLOW,
			true
		);

		(cast FlxG.state:PlayState).playerAttacks.add(sprite);

		return (cast FlxG.state:PlayState).engine.create([
			physicsBody,
			new DiesOffscreenComponent(),
			new PositionComponent(x - sprite.width/2, y - sprite.height/2),
			new SpriteComponent(sprite),
			new PlayerAttackComponent(10)
		]);
	}
}