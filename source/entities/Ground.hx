package entities;

import components.render.PositionComponent;
import components.render.SpriteComponent;
import states.PlayState;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.group.FlxGroup;
import flixel.util.FlxColor;

class Ground {
	static public var THICKNESS = 50;

	public static function create():FlxSprite {
		var sprite = new FlxSprite(0, PlayState.LEVEL_HEIGHT - THICKNESS);

		sprite.makeGraphic(
			Std.int(PlayState.LEVEL_WIDTH),
			THICKNESS,
			FlxColor.BROWN,
			true
		);

		sprite.solid = false;
		sprite.immovable = true;
		sprite.moves = false;

		return sprite;
	}
}
