package components.movement;

import edge.IComponent;
import flixel.math.FlxPoint;

class PhysicsBodyComponent implements IComponent {
	var acceleration:FlxPoint;
	var drag:FlxPoint;
	var immovable = false;
	var maxVelocity:FlxPoint;
	var velocity:FlxPoint;

	public function new() {
		this.acceleration = new FlxPoint();
		this.drag = new FlxPoint();
		this.maxVelocity = new FlxPoint();
		this.velocity = new FlxPoint();
	}
}
