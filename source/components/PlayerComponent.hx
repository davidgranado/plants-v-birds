package components;

import edge.IComponent;

class PlayerComponent implements IComponent {
	var speed:Float;
	var power:Float;

	public function new(speed:Float) {
		this.speed = speed;
		this.power = 0.0;
	}
}
