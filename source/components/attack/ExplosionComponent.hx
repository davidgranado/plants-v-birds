package components.attack;

import edge.IComponent;

class ExplosionComponent implements IComponent {
	var power:Float;
	var expanding = true;
	var currentRadius = 0.0;

	public function new(power:Float) {
		this.power = power;
	}
}
