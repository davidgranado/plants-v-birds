package components.attack;

import edge.IComponent;

class MachineGunComponent implements IComponent {
	var cooldownTime:Float;
	var remainingCooldownTime:Float = 0.0;

	public function new(cooldownTime:Float) {
		this.cooldownTime = cooldownTime;
	}
}
