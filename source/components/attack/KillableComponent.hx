package components.attack;

import edge.IComponent;
import flixel.math.FlxPoint;

class KillableComponent implements IComponent {
	public var life:Float;
	public var lifeRegenRate:Float;
}
