package components.attack;

import edge.IComponent;

class GunComponent implements IComponent {
	var automatic:Bool;
	var cooldownTime:Float;
	var remainingCooldownTime:Float = 0;
	var recoil:Float = 0;
	var recoilPower:Float = 10;
	var additionalRecoilCoefficient:Float = 0.5;
	var maxRecoil:Float = 30;

	public function new(cooldownTime:Float = 150, automatic:Bool = true) {
		this.cooldownTime = cooldownTime;
		this.automatic = automatic;
	}
}
