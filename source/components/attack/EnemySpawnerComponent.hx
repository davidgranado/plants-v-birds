package components.attack;

import edge.Entity;
import edge.IComponent;

class EnemySpawnerComponent implements IComponent {
	var spawnRate:Float = 2000; // ms
	var levelSpawnCount:Int;
	var remainingTimeToSpawn:Float = 0; // ms
	var level:Int;

	public function new() {}
}
